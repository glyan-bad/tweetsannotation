# TweetsAnnotation

**_This tool aims at annotating tweets quotes to help tweets classifiation._**

Based on Python 3, streamlit and pandas (mainly)


## Database
The last version of the annotated tweets database is in `current_db/` as `db.csv`

## Batches generation
the folder `batches_generation` contains a parquet file with 37k quotes. The attached notebook helps for the producing of annotating batches

Note that the database is somehow small due to the fact that we need to display the full text of all tweets. Albeit the legacy database contains more tweets, those who cannot be displayed online must be 140 characters long or less because the Twitter API has this character limitation for full text.


## Requirements

* Python 3
* pip3
* virtualenv
* Unix based environment preferred

### Your work

> Tweets quotes (tweets that react to other tweets) will be displayed *(on the left part of the screen)* to you along with a set of buttons *(on the right part of the screen)* that should be clicked to proceed to the next *(or previous)* tweet.

**Your work aims at analyzing the quoter's reaction and classifying it by the means of the set of buttons** *(cf classification specifications)*

**N.B:** Annotations are saved after each tweet is classified. If needed, you can change the class of any previous tweet using the button `PREVIOUS TWEET`. Also, whenever you need you can press the button `End and register annotation`, stop the app and resume your work later *(see Getting Started)*

---

### What you see if the tweet is online
![alt text](./ft.png "Full tweet")


---

### What you see if the tweet is no longer online
![alt text](./lt.png "Local teet")

---

### Classification specifications

<table>
<tr>
<th>Class</th>
<th>Fake alert</th>
<th>Debunk</th>
<th>General Discussion about fakes</th>
<th>Wondering if fake</th>
<th>Not a fake alert</th>
<th>Unrelated subject</th>
</tr>
<tr>
<th>Class id</th>
<th>0</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
</tr>
<tr>
<th>Class description</th>
<th>The quoter claims that the original is a fake</th>
<th>The quoter debunks or links to a debunking of the original</th>
<th>The quoter is discussing about fake news</th>
<th>The quoter wonders if the original is fake without confirming it</th>
<th>The quoter claims that the original is not a fake</th>
<th>The quoter's content is unrelated to the original or to fakes (or even troll)</th>
</tr>
</table>

## How the app works

It takes **parquet** files as input that should have this minimal header format :
<table>
<tr>
<th>id</th>
<th>text</th>
<th>full_text</th>
</tr>
<tr>
<th>tweets ids</th>
<th>quoter's text (reaction)</th>
<th>quoted tweet text (original text)</th>
</tr>
</table>

**N.B** text data is used when tweets cannot be displayed after being fetched online. Due to Twitter API limitation, only quotes that are up to 140 characters long are in the files ([Source](https://stackoverflow.com/questions/63540330/get-extended-full-text-tweets-in-twitter-api-v2)).

---

The program yields a **csv file** as an output, that is updated automatically.
It should have the following format:

<table>
<tr>
<th>tweet_id</th>
<th>author</th>
<th>cls</th>
<th>desc</th>
</tr>
<tr>
<th>tweets ids</th>
<th>author of the annotations</th>
<th>tweets classes ids</th>
<th>description of the tweets classes</th>
</tr>
</table>

### INPUTS
*c.f Getting started section for commands examples*
 
**mandatory**

```
  -i STR, --in STR      parquet file with the tweets to be annotated 
  -n STR, --name STR    name of the annotator
```

 **optional**
 
```
  -lw STR, --last_work STR csv file containing previous output
```

### OUTPUTS
for a given name ```NAME``` passed as ```-n``` or ```--name``` argument :

**A csv file NAME.csv**

## Getting Started

**Prepare environment**

```
virtualenv tweetannot
source tweetannot/bin/activate
pip3 install -r requirements.txt

```

**First Run**

> DON'T REFRESH (F5) IF YOU HAVE NOT SAVED YOUR WORK


```
NAME=yourname
streamlit run annotation.py -- -i file.parquet -n ${NAME}
```

**Run with previous work**

```
streamlit run annotation.py -- -i file.parquet -n ${NAME} -lw ${NAME}.csv
```

**In any case, reach** [http://localhost:8501](http://localhost:8501)
