import sys

import streamlit as st
import streamlit.components.v1 as components
import requests
import pandas as pd
import os, argparse

################################################################################
################################################################################

st.set_page_config(layout="wide")

parser = argparse.ArgumentParser()

parser.add_argument("-i", "--in", dest="file_input",
                    help="parquet file with the tweets to be annotated", metavar="STR")

parser.add_argument("-n", "--name", dest="annot_name",
                    help="name of the annotator", metavar="STR")

parser.add_argument("-lw", "--last_work", dest="last_work",
                    help="csv file containing previous output", metavar="STR")

args = parser.parse_args()

if not args.file_input or not args.annot_name:
    st.write("ERROR: Please enter valid arguments: Check terminal")
    print("Please enter valid arguments: \n")
    parser.print_help()
    sys.exit("Arguments error")
################################################################################
################################################################################

if 'num' not in st.session_state:
    st.session_state.num = 0
if 'data' not in st.session_state:
    st.session_state.data = {}
if 'lastCls' not in st.session_state:
    st.session_state.lastCls = {}
if 'loaded' not in st.session_state:
    st.session_state.loaded = False
if 'last_work' not in st.session_state:
    st.session_state.last_work = []

if args.last_work:
    st.session_state.last_work = pd.read_csv(args.last_work)['tweet_id'].to_list()

tweets_db = pd.read_parquet(args.file_input)
if len(st.session_state.last_work) > 0:
    tweets_db = tweets_db[~tweets_db['id'].isin(st.session_state.last_work)]


################################################################################
################################################################################

def theTweet(tweet_id):
    # print(tweet_id)
    # print(tweets_db[tweets_db['id'] == int(tweet_id)])
    try:
        url = "https://twitter.com/twitter/statuses/" + str(tweet_id)
        api = "https://publish.twitter.com/oembed?url={}".format(url)
        response = requests.get(api)
        res = response.json()["html"]
        return res
    except:
        return "<blockquote class=\"twitter-tweet\"><p lang=\"fr\" dir=\"ltr\"> <font color=\"red\"><b><u>REACTION</u></b></font>: " + \
               tweets_db[tweets_db['id'] == int(tweet_id)]['text'].values[0] + "</p><br/><br/>" + \
               "<p><font color=\"blue\"><b><u>QUOTE</u></b></font>: " + \
               tweets_db[tweets_db['id'] == int(tweet_id)]['full_text'].values[0] + "</p>" + "</blockquote>"


################################################################################
################################################################################

class NewTweet:
    classes = ['Fake alert', 'Debunk', 'General discussion about fakes', 'Wondering if fake', 'Not a fake alert',
               'Unrelated subject']

    def __init__(self, page_id, tweet_id, tot):
        self.tweet_id = tweet_id
        self.tweet = theTweet(str(self.tweet_id))
        st.title(f"Tweet N°{page_id + 1}/{tot}")
        components.html(self.tweet, height=700)
        # self.selected_class = self.classes.index(st.radio(
        #     "Tweet reaction class",
        #     self.classes, key=tweet_id,
        #     index=st.session_state.lastCls[tweet_id]))


def save(path, disp=False):
    result = list(map(lambda x: list(x.values()), list(st.session_state.data.values())))
    df = pd.DataFrame.from_records(result, columns=['tweet_id', 'author', 'cls', 'desc'])
    # save_name = path + args.annot_name + '.csv' if len(
    #   st.session_state.last_work) == 0 else path + args.annot_name + '_new.csv'
    if disp:
        save_name = path + args.annot_name + '.csv'
        print(result)
        st.dataframe(df)
        st.write("Saved here: " + save_name)
        if args.last_work:
            to_save = pd.concat([df, pd.read_csv(args.last_work)])
            to_save.to_csv(save_name, index_label=False, index=False)
        else:
            df.to_csv(save_name, index_label=False, index=False)
        os.remove(path + args.annot_name + '_new.csv')
    else:
        save_name = path + args.annot_name + '_new.csv'
        df.to_csv(save_name, index_label=False, index=False)


################################################################################
################################################################################

def main():
    tweets_ids = list(tweets_db['id'])
    nb_tweets = len(tweets_ids)
    placeholder_path = st.empty()
    placeholder2 = st.empty()
    placeholder = st.empty()
    path = placeholder_path.text_input(label="Output path:", value=os.getcwd() + '/')
    if not st.session_state.loaded:
        for i in tweets_ids:
            st.session_state.lastCls.update({i: 2})
        st.session_state.loaded = True

    while True:
        print("num ", st.session_state.num)
        if placeholder2.button('End and register annotation', key=st.session_state.num):
            placeholder2.empty()
            save(path, disp=True)
            break
        else:
            with placeholder.form(key=str(st.session_state.num)):
                col1, col2 = st.columns(2)
                with col1:
                    if st.session_state.num < nb_tweets:
                        new_tweet = NewTweet(page_id=st.session_state.num, tweet_id=tweets_ids[st.session_state.num],
                                             tot=nb_tweets)
                        with col2:
                            fa = st.form_submit_button('Fake alert')
                            debunk = st.form_submit_button('Debunk')
                            gd = st.form_submit_button('General discussion about fakes')
                            wond = st.form_submit_button('Wondering if fake')
                            naf = st.form_submit_button('Not a fake alert')
                            unrel = st.form_submit_button('Unrelated subject')
                            components.html(
                                """<hr style="height:10px;border:none;color:#333;background-color:#333;" /> """)
                            prev = st.form_submit_button('PREVIOUS TWEET')

                        if fa:
                            st.session_state.num += 1
                            st.session_state.data.update({new_tweet.tweet_id:
                                                              {'tweet_id': new_tweet.tweet_id,
                                                               'auhtor': args.annot_name,
                                                               'cls': 0,
                                                               'desc': 'Fake alert'}})
                            placeholder.empty()
                            placeholder2.empty()
                            save(path)

                        elif debunk:
                            st.session_state.num += 1
                            st.session_state.data.update({new_tweet.tweet_id:
                                                              {'tweet_id': new_tweet.tweet_id,
                                                               'auhtor': args.annot_name,
                                                               'cls': 1,
                                                               'desc': 'Debunk'}})
                            placeholder.empty()
                            placeholder2.empty()
                            save(path)

                        elif gd:
                            st.session_state.num += 1
                            st.session_state.data.update({new_tweet.tweet_id:
                                                              {'tweet_id': new_tweet.tweet_id,
                                                               'auhtor': args.annot_name,
                                                               'cls': 2,
                                                               'desc': 'General discussion about fakes'}})
                            placeholder.empty()
                            placeholder2.empty()
                            save(path)

                        elif wond:
                            st.session_state.num += 1
                            st.session_state.data.update({new_tweet.tweet_id:
                                                              {'tweet_id': new_tweet.tweet_id,
                                                               'auhtor': args.annot_name,
                                                               'cls': 3,
                                                               'desc': 'Wondering if fake'}})
                            placeholder.empty()
                            placeholder2.empty()
                            save(path)

                        elif naf:
                            st.session_state.num += 1
                            st.session_state.data.update({new_tweet.tweet_id:
                                                              {'tweet_id': new_tweet.tweet_id,
                                                               'auhtor': args.annot_name,
                                                               'cls': 4,
                                                               'desc': 'Not a fake alert'}})
                            placeholder.empty()
                            placeholder2.empty()
                            save(path)

                        elif unrel:
                            st.session_state.num += 1
                            st.session_state.data.update({new_tweet.tweet_id:
                                                              {'tweet_id': new_tweet.tweet_id,
                                                               'auhtor': args.annot_name,
                                                               'cls': 5,
                                                               'desc': 'Unrelated subject'}})
                            placeholder.empty()
                            placeholder2.empty()
                            save(path)

                        elif st.session_state.num > 0 and prev:
                            st.session_state.num -= 1
                            placeholder.empty()
                            placeholder2.empty()
                        else:
                            st.stop()
                    else:
                        if st.form_submit_button('End and register annotation'):
                            placeholder2.empty()
                            save(path, disp=True)
                            break
                        elif st.session_state.num > 0 and st.form_submit_button('PREVIOUS TWEET'):
                            st.session_state.num -= 1
                            placeholder.empty()
                            placeholder2.empty()
                        else:
                            st.stop()


if __name__ == '__main__':
    main()
